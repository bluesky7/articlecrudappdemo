package com.asif.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.asif.demo.model.Article;
import com.asif.demo.service.ArticleService;
import com.asif.demo.service.ArticleServiceHelper;

@Controller
@RequestMapping(value = "/article")
public class ArticleController {

	@Autowired
	ArticleService articleService;
	
	//@Autowired
	//ArticleServiceHelper serviceHelper;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView("article_list");
		List<Article> articleList = articleService.getAllArticles();
		model.addObject("articleList", articleList);
		
		System.out.println(articleList);
		
		return model;
	}

	@RequestMapping(value = "/addArticle/", method = RequestMethod.GET)
	public ModelAndView addArticle() {
		ModelAndView model = new ModelAndView();

		Article article = new Article();
		model.addObject("articleForm", article);
		model.setViewName("article_form");

		return model;
	}

	@RequestMapping(value = "/updateArticle/{id}", method = RequestMethod.GET)
	public ModelAndView editArticle(@PathVariable long id) {
		ModelAndView model = new ModelAndView();

		Article article = articleService.getArticleById(id);
		model.addObject("articleForm", article);
		model.setViewName("article_form");

		return model;
	}

	@RequestMapping(value = "/saveArticle", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("articleForm") Article article) {
		articleService.saveOrUpdate(article);

		return new ModelAndView("redirect:/article/list");
	}

	@RequestMapping(value = "/deleteArticle/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
		articleService.deleteArticle(id);

		return new ModelAndView("redirect:/article/list");
	}
	
	//===================================
	
	@RequestMapping(value = "/categoryList", method = RequestMethod.GET)
	public ModelAndView findByCategory(@RequestParam String category) {
		ModelAndView model = new ModelAndView("article_list");
	
		//List<Article> articleList = articleService.findByCategory("y");
		List<Article> articleList = articleService.findByCategory(category);
		model.addObject("articleList", articleList);
	
		System.out.println(articleList);
		
		return model;
	}
	
	@RequestMapping(value = "/qtyList", method = RequestMethod.GET)
	public ModelAndView findByQuantityGreaterThan(@RequestParam Double qty) {
		ModelAndView model = new ModelAndView("article_list");
	
		//List<Article> articleList = articleService.findByCategory("y");
		List<Article> articleList = articleService.findByQtyGreaterThan(qty);
		model.addObject("articleList", articleList);
	
		System.out.println(articleList);
		
		return model;
	}
	
	@RequestMapping(value = "/orderByQty", method = RequestMethod.GET)
	public ModelAndView findByCategorySorted(@RequestParam String category) {
		ModelAndView model = new ModelAndView("article_list");
	
		//List<Article> articleList = articleService.findByCategory("y");
		List<Article> articleList = articleService.findByCategorySorted(category);
		model.addObject("articleList", articleList);
	
		System.out.println(articleList);
		
		return model;
	}
	
	
	//========= This Query is OK =========
/*	@RequestMapping(value = "/maxSal", method = RequestMethod.GET)
	public ModelAndView findArticleWithHighestQty() {
		ModelAndView model = new ModelAndView("article_list");
		List<Article> articleList = serviceHelper.findArticleWithHighestQty();
		model.addObject("articleList", articleList);
		
		//System.out.println(articleList);
		
		return model;
	}*/
	
	@RequestMapping(value = "/maxQty", method = RequestMethod.GET)
	public ModelAndView findByMaxQty() {
		ModelAndView model = new ModelAndView("article_list");
		List<Article> articleList = articleService.nfindByMaxQty();
		model.addObject("articleList", articleList);	
		//System.out.println(articleList);
		
		return model;
	}
		
	
/*	@RequestMapping(value = "/maxQty", method = RequestMethod.GET)
	public ModelAndView findMaxQty() {
		ModelAndView model = new ModelAndView("article_list");
	
		//List<Article> articleList = articleService.findByCategory("y");
		List<Article> articleList = articleService.findMaxQty();
		model.addObject("articleList", articleList);
	
		System.out.println(articleList);
		
		return model;
	}*/
}
