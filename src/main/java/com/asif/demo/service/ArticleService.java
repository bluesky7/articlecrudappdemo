package com.asif.demo.service;

import java.util.List;

import com.asif.demo.model.Article;

public interface ArticleService 
{

	 public List<Article> getAllArticles();
	 
	 public Article getArticleById(long id);
	 
	 public void saveOrUpdate(Article article);
	 
	 public void deleteArticle(long id);
	 
	 
	 public List<Article> findByCategory (String category);

	 public List<Article> findByQtyGreaterThan(Double qty);

	 public List<Article> findByCategorySorted(String category);
	 
	 public List<Article> nfindByMaxQty();
	 
	 //public List<Article> findEmployeeWithHighestSalary();
	 
	 /*public List<Article> findMaxQty();*/
	 
}
