package com.asif.demo.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

import com.asif.demo.model.Article;

@Component
public class ArticleServiceHelper {

	// Create Session Factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Article.class)
			.buildSessionFactory();

	// Create Session
	Session session = factory.getCurrentSession();

	public List<Article> findArticleWithHighestQty() 
	{
		try 
		{
			//-----------------Using HQL----------------
			session.beginTransaction();

			List<Article> list = session.createQuery("from Article Where qty >=7 ").list();
			//displayStudents(theStudent);
			
			for (Article std : list) {
				System.out.println(std);
			}
/*
			theStudent = session.createQuery("from Student s Where s.firstName='Nila'").list();
			displayStudents(theStudent);

			theStudent = session.createQuery("from Student s Where s.firstName='Nila' OR lastName Like '%Ali%'").list();
			displayStudents(theStudent);
			*/
			
			session.getTransaction().commit();
			return list;

		} finally {
			factory.close();
		}
		
	}
}
