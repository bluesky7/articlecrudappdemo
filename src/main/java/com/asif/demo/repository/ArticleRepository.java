package com.asif.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.asif.demo.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Long>
{
	List<Article> findByCategory(String category);
	List<Article> findByQtyGreaterThan(Double qty);
	
	@Query("from Article where category=?1 order by qty desc")
	List<Article> findByCategorySorted(String category);
	
	
	@Query("from Article where qty in (SELECT max(qty) as qty from Article) order by title asc")
	List<Article> nfindByMaxQty();
	
	
/*	@Query(value = "SELECT max(Quantity) as qty from Article", nativeQuery = true)
	public List<Article> findMaxQty();*/

	//@Query(value = "SELECT max(age) from User where first_name <> ?1", nativeQuery = true)
    //int getMaxAgeMinus(String name);

}
